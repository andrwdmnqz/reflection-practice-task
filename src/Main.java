import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException {
        Animal animal1 = new Animal("Name1", 21, "grey");
        Cat cat1 = new Cat("Name2", 22, "brown");
        cat1.changeAnimalFields(animal1, cat1.getName(), cat1.getAge(), cat1.getColor());

        String scannedLine = "";
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.println("Enter class that you want to display (there are two available - Cat and Animal): ");
            scannedLine = sc.nextLine();

            if(cat1.getClass().getName().equals(scannedLine)) {
                System.out.println(" Class: " + cat1.getClass().getName() + ", it's fields: ");

                for(Field fields: cat1.getClass().getDeclaredFields()) {
                    fields.setAccessible(true);
                    System.out.println(fields.getType().getName() + ", it's name is " + fields.getName() + ", it's value is " + fields.get(cat1));
                }
                System.out.println("\n It's methods: ");
                for(Method methods: cat1.getClass().getDeclaredMethods()) {
                    methods.setAccessible(true);
                    System.out.println(methods);
                }
                break;
            }
            if(animal1.getClass().getName().equals(scannedLine)) {
                System.out.println(" Class: " + animal1.getClass().getName() + ", it's fields: ");

                for(Field fields: animal1.getClass().getDeclaredFields()) {
                    fields.setAccessible(true);
                    System.out.println(fields.getType().getName() + ", it's name is " + fields.getName() + ", it's value is " + fields.get(animal1));
                }
                System.out.println("\n It's methods: ");
                for(Method methods: animal1.getClass().getDeclaredMethods()) {
                    methods.setAccessible(true);
                    System.out.println(methods);
                }
                break;
            }
            else {
                System.out.println("Wrong class! Try again");
                continue;
            }
        }
    }
}