import java.lang.reflect.Field;

public class Cat extends Animal {
    private String name1 = "exampleName";

    public Cat(String name, int age, String color) {
        super(name, age, color);
    }

    public void changeAnimalFields(Animal animal, String name, int age, String color) throws IllegalAccessException {

        Class<? extends Animal> animalClass = animal.getClass();
        for(Field fields: animalClass.getDeclaredFields()) {
            if(fields.getName().equals("name")) {
                System.out.println("Changed animal name " + fields.get(animal) + " to " + name);
                fields.set(animal, name);
            }
            if(fields.getName().equals("age")) {
                fields.setAccessible(true);
                System.out.println("Changed animal age field from " + fields.get(animal) + " to " + age);
                fields.set(animal, age);
            }
            if(fields.getName().equals("color")) {
                fields.setAccessible(true);
                System.out.println("Changed animal color field from " + fields.get(animal) + " to " + color);
                fields.set(animal, color);
            }
        }
    }
}
